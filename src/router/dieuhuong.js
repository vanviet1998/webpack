import React, { PureComponent } from 'react'
import User from '../component/user'
import Task from '../component/task'
import Newtask from '../component/newtask'
import { Route,HashRouter } from "react-router-dom"

export default class dieuhuong extends PureComponent {
    render() {
        return (
            <HashRouter>
                <Route exact path="/" render={() => <User />} />
                <Route path="/task" render={() => <Task />} />
                <Route path="/newtask" render={() => <Newtask />} />
            </HashRouter>
        )
    }
}