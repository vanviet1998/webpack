import React, { PureComponent } from 'react'
import Header from './component/header'
import Dieuhuong from './router/dieuhuong'
import { BrowserRouter } from "react-router-dom";

export default class App extends PureComponent {
  render() {
    return (
      <BrowserRouter>
        <Header />
        <div className="container">
          <Dieuhuong />
        </div>
      </BrowserRouter>
    );
  }
}

