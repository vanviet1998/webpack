import React, { PureComponent } from 'react'
import { Link } from "react-router-dom";
import New from './detailTask/new'
import Process from './detailTask/proces'
import Resole from './detailTask/resole'
import { connect } from 'react-redux'
import { getUser } from '../redux/action/userAction'
import PropTypes from 'prop-types';
import { editTask } from '../redux/action/taskAction'

class task extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            date: '',
            age: '',
            sex: '',
            id: '',
            nameTask: '',
            detailTask: ''
        }
    }
    componentWillMount() {
        this.setState({
            name: this.props.userTask.name,
            date: this.props.userTask.date,
            age: this.props.userTask.age,
            sex: this.props.userTask.age
        })

    }

    getEditTask = (task) => {
        console.log(task)
        this.setState({
            id: task.id,
            nameTask: task.nameTask,
            detailTask: task.detailTask,
            typeTask: task.typeTask
        })

    }
    getDetailUser = (event) => {
        let name = event.target.name
        let val = event.target.value
        this.setState({
            [name]: val
        })

    }
    editUser = () => {
        let user = {
            name: this.state.name,
            date: this.state.date,
            age: this.state.age,
            sex: this.state.sex
        }
        console.log(this.state)
        this.props.getUser(user)
    }
    getDetailTask = (event) => {
        let name = event.target.name
        let val = event.target.value
        this.setState({
            [name]: val,
        })
    }
    clickEdit = () => {
        let task = {
            id: this.state.id,
            nameTask: this.state.nameTask,
            detailTask: this.state.detailTask,
            typeTask: this.state.typeTask
        }
        this.props.editTask(task)
    }

    render() {
        return (
            <div>
                <div className="thongtin float-right">
                    <a href="#255" data-toggle="modal" data-target="#myModal" className="btn btn-outline-primary">Hello:{this.props.userTask.name}</a>
                </div>
                <Link to="/newtask" className="btn btn-outline-primary">News task</Link >
                <div className="row mt-2">
                    <div className="col-4">
                        <div className="card">
                            <div className="card-header">
                                New
                    </div>
                            {
                                this.props.task.map((val, index) => {
                                    if (val.typeTask === 'new') {
                                        return (
                                            <New key={index} getEditTask={(task) => this.getEditTask(task)} id={val.id} typeTask={val.typeTask} name={val.nameTask} detail={val.detailTask}></New>
                                        )
                                    }else{
                                        return null
                                    }
                                })

                            }
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="card">
                            <div className="card-header">
                                In Process
                    </div>
                            {
                                this.props.task.map((val, index) => {
                                    if (val.typeTask === 'proces') {
                                        return (
                                            <Process key={index} getEditTask={(task) => this.getEditTask(task)} id={val.id} typeTask={val.typeTask} name={val.nameTask} detail={val.detailTask}></Process>
                                        )
                                    }else return null
                                })
                            }
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="card">
                            <div className="card-header">
                                Resolved
                    </div>
                            {
                                this.props.task.map((val, index) => {
                                    if (val.typeTask === 'resoled') {
                                        return (
                                            <Resole key={index} getEditTask={(task) => this.getEditTask(task)} id={val.id} typeTask={val.typeTask} name={val.nameTask} detail={val.detailTask}></Resole>
                                        )
                                    }else return null
                                })
                            }
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="myModal" role="dialog">
                    <div className="modal-dialog">
                        {/* Modal content*/}
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Detail User</h4>
                                <button type="button" className="close" data-dismiss="modal">×</button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label >Name</label>
                                        <input type="text" name='name' onChange={(event) => this.getDetailUser(event)} className="form-control" defaultValue={this.props.userTask.name} aria-describedby="helpId" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputPassword1" >Date of Birth</label>
                                        <input type="date" name='date' onChange={(event) => this.getDetailUser(event)} defaultValue={this.props.userTask.date} className="form-control" id="exampleInputPassword1" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputPassword1"  >Age</label>
                                        <input type="number" name='number' onChange={(event) => this.getDetailUser(event)} defaultValue={this.props.userTask.age} className="form-control" id="exampleInputPassword1" />
                                    </div>
                                    <div className="form-group">
                                        <select name='sex' onChange={(event) => this.getDetailUser(event)} defaultValue={this.props.userTask.sex
                                        } className="form-control">
                                            <option value="true" >Nam</option>
                                            <option value="false">Nu</option>
                                        </select>

                                    </div>
                                    <a className="btn btn-default btn btn-outline-primary" data-dismiss="modal" href='#000' onClick={() => this.editUser()}>Submit</a>
                                    <button type="reset" className="btn btn-outline-primary">Reset</button>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* modal Task */}
                <div className="modal fade" id="myModal1" role="dialog">
                    <div className="modal-dialog">
                        {/* Modal content*/}
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Detail User</h4>
                                <button type="button" className="close" data-dismiss="modal">×</button>
                            </div>
                            <div className="modal-body">
                                <form action="/" style={{ maxWidth: '400px', margin: 'auto' }}>
                                    <div className="form-group">
                                        <label >Name task</label>
                                        <input type="text" name='nameTask' onChange={(event) => this.getDetailTask(event)} value={this.state.nameTask} className="form-control" aria-describedby="helpId" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputPassword1">Detail</label>
                                        <textarea name='detailTask' onChange={(event) => this.getDetailTask(event)} className="form-control" value={this.state.detailTask} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputPassword1">creator:{this.props.userTask.name}</label>
                                    </div>
                                    <div className="form-group">
                                        <select name='typeTask' onChange={(event) => this.getDetailTask(event)} value={this.state.typeTask} className="form-control">
                                            <option value="new" >New</option>
                                            <option value="proces">In Process</option>
                                            <option value="resoled">resoled</option>
                                        </select>

                                    </div>
                                    <a href='#4' onClick={() => this.clickEdit()} data-dismiss="modal" className="btn btn-outline-primary">Submit</a>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        userTask: state.user.user,
        task: state.taskReducer.task
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getUser: (user) => {
            dispatch(getUser(user))
        },
        editTask: (task) => {
            dispatch(editTask(task))
        }
    }
}
task.propTypes = {
    task: PropTypes.array,
    user: PropTypes.object,
    getTask: PropTypes.func,
    editTask: PropTypes.func
}
export default connect(mapStateToProps, mapDispatchToProps)(task)