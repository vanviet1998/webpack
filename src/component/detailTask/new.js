import React, { PureComponent } from 'react'

export default class componentName extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            id: '', nameTask: '', detailTask: '', typeTask: ''
        }
    }
    clickEdit = async () => {

        await this.setState({
            id: this.props.id,
            nameTask: this.props.name,
            detailTask: this.props.detail,
            typeTask: this.props.typeTask
        })
        this.props.getEditTask(this.state)
    }
    render() {
        return (
            <ul className="list-group list-group-flush">
                <li className="list-group-item "> {this.props.name}
                    <div className="congcu float-right ml-4">
                        <button onClick={() => this.clickEdit()} type="button" data-toggle="modal" data-target="#myModal1" className="btn btn-danger">Edit</button>
                    </div>
                </li>
            </ul>
        )
    }

}
