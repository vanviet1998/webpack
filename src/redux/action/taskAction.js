import { Get_TASK,EDIT_TASK } from '../typeAction'

export function getTask(task) {
    return { type: Get_TASK, task }
}
export function editTask(task) {
    return { type: EDIT_TASK, task }
}
