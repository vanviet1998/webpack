import { Get_USER } from '../typeAction'

export function getUser(User) {
    return { type: Get_USER, User }
}