import { Get_TASK, EDIT_TASK } from '../typeAction'

const initialState = {
    task: []
};

export function taskReducer1(state = initialState, action) {
    switch (action.type) {
        case Get_TASK:
            return {
                ...state,
                task: state.task.concat(action.task)
            }
        case EDIT_TASK:
            let newTask = [...state.task].filter(item => item.id !== action.task.id)
            newTask = [...newTask, action.task]
            return {
                ...state,
                task: newTask
            }
        default:
            return state
    }
}