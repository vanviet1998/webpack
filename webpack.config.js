const path = require('path')
const webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: {
        bundle: ["babel-polyfill", './src/index.js'],
    }, output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                use: 'babel-loader',
                test: /\.js$/,
                exclude: '/node_modules/'
            }, {
                use: [
                    'style-loader',
                    'css-loader'
                ],
                test: /\.css$/
            }, {
                loader: 'file-loader',
                test: /\.jpe?g&|\.gif&|\.png$|\svg$|\.svg$|.woff$|\.woff2/
            }
        ]
    },
    optimization: {
        splitChunks: {
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name: 'vendor',
              chunks: 'all',
            }
          }
        }
      },
      plugins: [new HtmlWebpackPlugin({
          template:'index.html'
      })]
}